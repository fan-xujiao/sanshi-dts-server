/*
SQLyog Ultimate v12.5.0 (64 bit)
MySQL - 8.0.18 : Database - dts
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dts` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `dts`;

/*Table structure for table `t_project` */

DROP TABLE IF EXISTS `t_project`;

CREATE TABLE `t_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(20) DEFAULT NULL COMMENT '项目名称',
  `design_path` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '设计图存储路径',
  `app_key` varchar(128) DEFAULT NULL COMMENT '项目appkey',
  `app_secret` varchar(128) DEFAULT NULL COMMENT '项目密钥',
  `note` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '项目说明',
  `creater_id` int(11) DEFAULT NULL COMMENT '创建者id',
  `gmt_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '项目创建时间',
  `gmt_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '项目编辑时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目表';

/*Data for the table `t_project` */

/*Table structure for table `t_project_alarm` */

DROP TABLE IF EXISTS `t_project_alarm`;

CREATE TABLE `t_project_alarm` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `project_id` int(11) DEFAULT NULL COMMENT '项目id',
  `monitor_id` int(11) DEFAULT NULL COMMENT '监听id',
  `prop_key` varchar(50) DEFAULT NULL COMMENT '属性key',
  `state` tinyint(1) DEFAULT '0' COMMENT '状态：0-正在报警，1-已结束',
  `value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '报警值',
  `msg` varchar(100) DEFAULT NULL COMMENT '报警内容',
  `gmt_trigger` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '报警时间',
  `gmt_release` timestamp NULL DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`),
  KEY `host_id` (`project_id`),
  CONSTRAINT `t_project_alarm_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `t_project_monitor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目报警日志';

/*Data for the table `t_project_alarm` */

/*Table structure for table `t_project_auth` */

DROP TABLE IF EXISTS `t_project_auth`;

CREATE TABLE `t_project_auth` (
  `project_id` int(11) NOT NULL COMMENT '项目id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `is_monitor` tinyint(1) DEFAULT '1' COMMENT '是否有监视权限',
  `is_edit` tinyint(1) DEFAULT '1' COMMENT '是否有编辑权限',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
  PRIMARY KEY (`project_id`,`user_id`),
  KEY `fk_auth_user_id` (`user_id`),
  CONSTRAINT `fk_auth_project_id` FOREIGN KEY (`project_id`) REFERENCES `t_project` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目授权表';

/*Data for the table `t_project_auth` */

/*Table structure for table `t_project_monitor` */

DROP TABLE IF EXISTS `t_project_monitor`;

CREATE TABLE `t_project_monitor` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '监听id',
  `project_id` int(11) DEFAULT NULL COMMENT '项目id',
  `template_id` int(11) DEFAULT NULL COMMENT '模板id',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '监听名称',
  `flag` varchar(32) DEFAULT NULL COMMENT '主机标识',
  `ip` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '监听ip',
  `notes` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '监听备注',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  KEY `fk_project_monitor` (`project_id`),
  KEY `index_monitor_template` (`template_id`),
  CONSTRAINT `fk_project_monitro` FOREIGN KEY (`project_id`) REFERENCES `t_project` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目监听表';

/*Data for the table `t_project_monitor` */

/*Table structure for table `t_template` */

DROP TABLE IF EXISTS `t_template`;

CREATE TABLE `t_template` (
  `id` int(11) NOT NULL COMMENT '主机id',
  `title` varchar(50) DEFAULT NULL COMMENT '监听模板',
  `note` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `gmt_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='监听模板表';

/*Data for the table `t_template` */

/*Table structure for table `t_template_prop` */

DROP TABLE IF EXISTS `t_template_prop`;

CREATE TABLE `t_template_prop` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `template_id` int(11) DEFAULT NULL COMMENT '模板id',
  `title` varchar(50) DEFAULT NULL COMMENT '属性名称',
  `prop_key` varchar(50) DEFAULT NULL COMMENT '属性key',
  `data_type` varchar(20) DEFAULT NULL COMMENT '数据类型',
  `data_length` varchar(20) DEFAULT NULL COMMENT '数据长度',
  `default_value` varchar(50) DEFAULT NULL COMMENT '默认值',
  `is_unsigned` tinyint(1) DEFAULT NULL COMMENT '无符号',
  `note` varchar(100) DEFAULT NULL COMMENT '备注',
  `gmt_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  KEY `fk_template_prop` (`template_id`),
  CONSTRAINT `fk_template_prop` FOREIGN KEY (`template_id`) REFERENCES `t_template` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='监听模板属性表';

/*Data for the table `t_template_prop` */

/*Table structure for table `t_template_trigger` */

DROP TABLE IF EXISTS `t_template_trigger`;

CREATE TABLE `t_template_trigger` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '戳发器id',
  `template_id` int(11) DEFAULT NULL COMMENT '主机id',
  `prop_key` varchar(50) DEFAULT NULL COMMENT '属性key',
  `judgment_symbol` varchar(10) DEFAULT NULL COMMENT '判断符号',
  `judgment_value` varchar(20) DEFAULT NULL COMMENT '判断值',
  `msg` varchar(200) DEFAULT NULL COMMENT '报警消息',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  KEY `fk_template_trigger` (`template_id`),
  CONSTRAINT `fk_template_trigger` FOREIGN KEY (`template_id`) REFERENCES `t_template` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='监听模板触发器表';

/*Data for the table `t_template_trigger` */

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `account` varchar(20) DEFAULT NULL COMMENT '账号',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `role_type` varchar(20) DEFAULT NULL COMMENT '角色类型：admin-超管，user-用户',
  `full_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '姓名',
  `department` varchar(20) DEFAULT NULL COMMENT '部门',
  `title` varchar(20) DEFAULT NULL COMMENT '职务',
  `telphone` varchar(20) DEFAULT NULL COMMENT '电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `gmt_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_user_account` (`account`) COMMENT '用户名唯一'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='用户表';

/*Data for the table `t_user` */

insert  into `t_user`(`id`,`account`,`password`,`role_type`,`full_name`,`department`,`title`,`telphone`,`email`,`gmt_create`,`gmt_modified`) values 
(1,'admin','A92CFE8C490B0210CFD2320D352E9E6AAC9B37A654A943125DA57F229AFC8821','admin','三石','IT部','超级管理员','',NULL,'2021-09-19 21:03:58','2021-09-19 21:20:42'),
(2,'sanshi','EF797C8118F02DFB649607DD5D3F8C7623048C9C063D532CC95C5ED7A898A64F',NULL,'三石','研发','软件工程师','987654321','987654321@qq.com','2021-09-20 22:03:35','2021-09-20 22:05:12');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
