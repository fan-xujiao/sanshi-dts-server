package com.dts.server.dbutil.entity;

import java.util.Date;

public class TemplateTrigger {
    private Integer id;

    private Integer templateId;

    private String propKey;

    private String judgmentSymbol;

    private String judgmentValue;

    private String msg;

    private Date gmtCreate;

    private Date gmtModified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public String getPropKey() {
        return propKey;
    }

    public void setPropKey(String propKey) {
        this.propKey = propKey == null ? null : propKey.trim();
    }

    public String getJudgmentSymbol() {
        return judgmentSymbol;
    }

    public void setJudgmentSymbol(String judgmentSymbol) {
        this.judgmentSymbol = judgmentSymbol == null ? null : judgmentSymbol.trim();
    }

    public String getJudgmentValue() {
        return judgmentValue;
    }

    public void setJudgmentValue(String judgmentValue) {
        this.judgmentValue = judgmentValue == null ? null : judgmentValue.trim();
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg == null ? null : msg.trim();
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}