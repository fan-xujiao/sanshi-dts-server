package com.dts.server.dbutil.entity;

import java.util.Date;
import java.util.Map;

public class MonitorData {
	
	private Integer monitorId;

    private Date time;
    
    private Map<String,Object> props;

	public Integer getMonitorId() {
		return monitorId;
	}

	public void setMonitorId(Integer monitorId) {
		this.monitorId = monitorId;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Map<String, Object> getProps() {
		return props;
	}

	public void setProps(Map<String, Object> props) {
		this.props = props;
	}

	public MonitorData() {
		super();
	}

	public MonitorData(Integer monitorId, Map<String, Object> props) {
		super();
		this.monitorId = monitorId;
		this.time = new Date();
		this.props = props;
	}
    
}
