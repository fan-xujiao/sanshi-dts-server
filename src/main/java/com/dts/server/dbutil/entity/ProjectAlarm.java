package com.dts.server.dbutil.entity;

import java.util.Date;

public class ProjectAlarm {
    private Integer id;

    private Integer projectId;

    private Integer monitorId;

    private String propKey;

    private Boolean state;

    private String value;

    private String msg;

    private Date gmtTrigger;

    private Date gmtRelease;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public Integer getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Integer monitorId) {
        this.monitorId = monitorId;
    }

    public String getPropKey() {
        return propKey;
    }

    public void setPropKey(String propKey) {
        this.propKey = propKey == null ? null : propKey.trim();
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value == null ? null : value.trim();
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg == null ? null : msg.trim();
    }

    public Date getGmtTrigger() {
        return gmtTrigger;
    }

    public void setGmtTrigger(Date gmtTrigger) {
        this.gmtTrigger = gmtTrigger;
    }

    public Date getGmtRelease() {
        return gmtRelease;
    }

    public void setGmtRelease(Date gmtRelease) {
        this.gmtRelease = gmtRelease;
    }
}