package com.dts.server.dbutil.entity;

public class ProjectAuthKey {
    private Integer projectId;

    private Integer userId;

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

	public ProjectAuthKey(Integer projectId, Integer userId) {
		super();
		this.projectId = projectId;
		this.userId = userId;
	}

	public ProjectAuthKey() {
		super();
	}
    
}