package com.dts.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;

@EnableScheduling
@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
@MapperScan("com.dts.server")
public class Application extends SpringBootServletInitializer{

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) { 
	    //注意这里的参数要指向原先用main方法执行的Application启动类 
	    return builder.sources(Application.class); 
	}
	 
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
