package com.dts.server.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dts.server.common.base.BaseQuery;
import com.dts.server.common.base.BaseSession;
import com.dts.server.common.enums.ResultCode;
import com.dts.server.common.enums.RoleType;
import com.dts.server.common.exception.CustomException;
import com.dts.server.common.result.ListResult;
import com.dts.server.common.util.PagerUtil;
import com.dts.server.db.dao.TemplateMapper;
import com.dts.server.dbutil.entity.Template;

/**
 * @ClassName:  TemplateService
 * @author 范旭蛟
 * @date 2021年10月17日 下午9:32:30
 * @Description: 监听模板管理Service
 */
@Service
@Transactional
public class TemplateService {
	

	@Resource
	private TemplateMapper templateMapper;

	
	/**
	 * 查询监听模板列表
	 * @param param
	 * @param session
	 * @return 
	 * @throws Exception
	 */
	public ListResult<Template> query(BaseQuery param, BaseSession session) throws Exception {
		Integer totalCount = templateMapper.queryCount(param);
		if (totalCount <= 0) {
			throw new CustomException(ResultCode.S_NULL_ERROR);
		}
		param.setStart(PagerUtil.MathStart(param.getPagenum(), totalCount));
		List<Template> list = templateMapper.query(param);
		return new ListResult<Template>(totalCount, list);
	}

	/**
	 * 删除监听模板
	 * @param id
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean delete(Integer id, BaseSession session) throws Exception {
		if(!RoleType.ADMIN.equals(session.getRoleType())){
			throw new CustomException(ResultCode.S_UNAUTH_ERROR);
		}
		return templateMapper.delete(id) > 0;
	}
	
	/**
	 * 新增监听模板
	 * @param template
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean add(Template template, BaseSession session) throws Exception {
		if(!RoleType.ADMIN.equals(session.getRoleType())){
			throw new CustomException(ResultCode.S_UNAUTH_ERROR);
		}
		return templateMapper.add(template) > 0;
	}
	
	/**
	 * 编辑监听模板信息
	 * @param template
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean edit(Template template, BaseSession session) throws Exception {
		if(!RoleType.ADMIN.equals(session.getRoleType())){
			throw new CustomException(ResultCode.S_UNAUTH_ERROR);
		}
		return templateMapper.edit(template) > 0;
	}
}
