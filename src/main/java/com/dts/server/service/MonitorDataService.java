package com.dts.server.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.dts.server.common.enums.ResultCode;
import com.dts.server.common.exception.CustomException;
import com.dts.server.db.dao.HistoryMapper;
import com.dts.server.db.dao.ProjectMonitorMapper;
import com.dts.server.db.dao.TemplateTriggerMapper;
import com.dts.server.dbutil.entity.MonitorData;
import com.dts.server.dbutil.entity.ProjectMonitor;
import com.dts.server.dbutil.entity.TemplateTrigger;

@Service
@Transactional
public class MonitorDataService {
	
	/**
	 * 监听session
	 * @ClassName:  MonitorSession
	 * @author 范旭蛟
	 * @date 2021年10月30日 下午8:55:59
	 * @Description: TODO(描述这个类的作用)
	 */
	class MonitorSession{
		
		private Integer monitorId; //监听id
		
		private List<TemplateTrigger> triggers; //触发器
		
		private Long expire; //到期时间
		
		public Integer getMonitorId() {
			return monitorId;
		}

		public List<TemplateTrigger> getTriggers() {
			return triggers;
		}

		public Long getExpire() {
			return expire;
		}

		public MonitorSession(Integer monitorId, List<TemplateTrigger> triggers) {
			super();
			this.monitorId = monitorId;
			this.triggers = triggers;
			this.expire = new Date().getTime() + 7200 * 1000;
		}
	}

	private Map<String, MonitorSession> tokenMap = new ConcurrentHashMap<>();
	
	private Map<Integer, MonitorData> dataMap = new ConcurrentHashMap<>();
	
	@Resource
	private ProjectMonitorMapper monitorMapper;
	
	@Resource
	private TemplateTriggerMapper triggerMapper;
	
	@Resource
	private HistoryMapper historyMapper;
	
	/**
	 * 获取token
	 * @param flag
	 * @return
	 * @throws Exception
	 */
	public String getToken(String flag) throws Exception{
		if(flag == null){
			throw new CustomException(ResultCode.PARAM_MISSING);
		}
		ProjectMonitor monitor = monitorMapper.selectByFlag(flag);
		if(monitor == null){
			throw new CustomException(ResultCode.PARAM_MISSING);
		}
		List<TemplateTrigger> triggers = triggerMapper.query(monitor.getTemplateId());
		String token = UUID.randomUUID().toString().replace("-", "");
		MonitorSession session = new MonitorSession(monitor.getId(), triggers);
		tokenMap.put(token, session);
		return token;
	}

	/**
	 * 新增数据
	 * @param data
	 * @return
	 * @throws Exception
	 */
	@DS("history")
	public void append(String token, Map<String,Object> props) throws Exception {
		MonitorSession session = tokenMap.get(token);
		if(session == null || session.getExpire() >new Date().getTime()){
			throw new CustomException(ResultCode.TOKEN_INVALID);
		}
		MonitorData data = new MonitorData(session.getMonitorId(), props);
		//增加实时数据
		dataMap.put(session.getMonitorId(), data);
		//增加历史数据
		historyMapper.append(data);
		//校验历史数据，并返回校验结果
		//return vaild(session.getTriggers(), data);
	}
	
}
