package com.dts.server.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.dts.server.db.dao.HistoryTableMapper;
import com.dts.server.db.entity.MyTemplateProp;
import com.dts.server.dbutil.entity.TemplateProp;

@Service
@Transactional
@DS("history")
public class HistoryTableService {

	@Resource
	private HistoryTableMapper historyTableMapper;
	
	/**
	 * 创建历史数据表
	 * @param id
	 * @param props
	 * @return
	 * @throws Exception
	 */
	public boolean createTable(Integer id, List<TemplateProp> props) throws Exception{
		return historyTableMapper.createTable(id, props) > 0;
	}
	
	/**
	 * 删除历史数据表
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public boolean dropTable(Integer id) throws Exception{
		return historyTableMapper.dropTable(id) > 0;
	}
	
	/**
	 * 向历史数据表新增字段
	 * @param id
	 * @param prop
	 * @return
	 * @throws Exception
	 */
	public boolean addColumn(Integer id, TemplateProp prop) throws Exception{
		return historyTableMapper.addColumn(id, prop) > 0;
	}
	
	/**
	 * 编辑历史数据表中的字段
	 * @param id
	 * @param prop
	 * @return
	 * @throws Exception
	 */
	public boolean editColumn(Integer id, MyTemplateProp prop) throws Exception{
		return historyTableMapper.editColumn(id, prop) > 0;
	}
	
	/**
	 * 删除历史数据表中的字段
	 * @param id
	 * @param propKey
	 * @return
	 * @throws Exception
	 */
	public boolean dropColumn(Integer id, String propKey) throws Exception{
		return historyTableMapper.dropColumn(id, propKey) > 0;
	}
}
