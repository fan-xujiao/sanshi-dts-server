package com.dts.server.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.dts.server.common.enums.ResultCode;
import com.dts.server.common.exception.CustomException;
import com.dts.server.common.result.ListResult;
import com.dts.server.common.util.PagerUtil;
import com.dts.server.db.dao.HistoryMapper;
import com.dts.server.query.HistoryQuery;

@Service
@Transactional
@DS("history")
public class HistoryService {
	
	@Resource
	private HistoryMapper historyMapper;
	
	/**
	 * 分页查询
	 * @param param
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public ListResult<Map<String,Object>> query(HistoryQuery param) throws Exception {
		Integer totalCount = historyMapper.queryCount(param);
		if (totalCount <= 0) {
			throw new CustomException(ResultCode.S_NULL_ERROR);
		}
		param.setStart(PagerUtil.MathStart(param.getPagenum(), totalCount));
		List<Map<String,Object>> list = historyMapper.query(param);
		return new ListResult<Map<String,Object>>(totalCount, list);
	}
	
	/**
	 * 根据条件，查询所有数据
	 * @param param
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public List<Map<String,Object>> queryAll(HistoryQuery param) throws Exception {
		List<Map<String,Object>> list = historyMapper.queryAll(param);
		if (list == null || list.size() == 0) {
			throw new CustomException(ResultCode.S_NULL_ERROR);
		}
		return list;
	}
	
	/**
	 * 清除数据
	 * @param param
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean clear(HistoryQuery param) throws Exception {
		return historyMapper.clear(param) > 0 ;
	}
}
