package com.dts.server.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dts.server.common.base.BaseSession;
import com.dts.server.common.enums.ResultCode;
import com.dts.server.common.enums.RoleType;
import com.dts.server.common.exception.CustomException;
import com.dts.server.db.dao.TemplateTriggerMapper;
import com.dts.server.dbutil.entity.TemplateTrigger;

/**
 * @ClassName:  TemplateTriggerService
 * @author 范旭蛟
 * @date 2021年10月17日 下午9:32:30
 * @Description: 模板触发器管理Service
 */
@Service
@Transactional
public class TemplateTriggerService {
	

	@Resource
	private TemplateTriggerMapper templateTriggerMapper;

	
	/**
	 * 查询模板触发器列表
	 * @param param
	 * @param session
	 * @return 
	 * @throws Exception
	 */
	public List<TemplateTrigger> query(Integer templateId, BaseSession session) throws Exception {
		return templateTriggerMapper.query(templateId);
	}

	/**
	 * 删除模板触发器
	 * @param id
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean delete(Integer id, BaseSession session) throws Exception {
		if(!RoleType.ADMIN.equals(session.getRoleType())){
			throw new CustomException(ResultCode.S_UNAUTH_ERROR);
		}
		return templateTriggerMapper.delete(id) > 0;
	}
	
	/**
	 * 新增模板触发器
	 * @param trigger
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean add(TemplateTrigger trigger, BaseSession session) throws Exception {
		if(!RoleType.ADMIN.equals(session.getRoleType())){
			throw new CustomException(ResultCode.S_UNAUTH_ERROR);
		}
		return templateTriggerMapper.add(trigger) > 0;
	}
	
	/**
	 * 编辑模板触发器信息
	 * @param trigger
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean edit(TemplateTrigger trigger, BaseSession session) throws Exception {
		if(!RoleType.ADMIN.equals(session.getRoleType())){
			throw new CustomException(ResultCode.S_UNAUTH_ERROR);
		}
		return templateTriggerMapper.edit(trigger) > 0;
	}
}
