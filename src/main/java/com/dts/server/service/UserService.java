package com.dts.server.service;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.druid.util.StringUtils;
import com.dts.server.common.base.BaseQuery;
import com.dts.server.common.base.BaseSession;
import com.dts.server.common.enums.ResultCode;
import com.dts.server.common.enums.RoleType;
import com.dts.server.common.exception.CustomException;
import com.dts.server.common.result.ListResult;
import com.dts.server.common.util.EncryptUtil;
import com.dts.server.common.util.PagerUtil;
import com.dts.server.db.dao.UserMapper;
import com.dts.server.db.entity.MyUser;
import com.dts.server.query.LoginQuery;

/**
 * @ClassName:  UserService
 * @author 范旭蛟
 * @date 2021年9月16日 下午9:17:13
 * @Description: 用户管理服务层
 */
@Service
@Transactional
public class UserService {
	

	@Resource
	private UserMapper userMapper;

	/**
	 * 用户登录
	 * @param param 用户名及密码
	 * @param session
	 * @return code=200则登录成功，否则登录失败
	 * @throws Exception
	 */
	public ResultCode login(LoginQuery param, HttpSession session) throws Exception {
		if(StringUtils.isEmpty(param.getAccount())){
			return ResultCode.ACCOUNT_NULL;
		}
		MyUser user = userMapper.login(param);
		if(user == null){
			return ResultCode.PASSWORD_INVALID;
		}
		session.setAttribute("userId", user.getId());
		session.setAttribute("account", user.getAccount());
		session.setAttribute("roleType", user.getRoleType());
		session.setAttribute("fullName", user.getFullName());
		session.setAttribute("department", user.getDepartment());
		session.setAttribute("title", user.getTitle());
		session.setAttribute("telphone", user.getTelphone());
		session.setAttribute("email", user.getEmail());
		session.setAttribute("gmtCreate", user.getGmtCreate());
		return ResultCode.SUCCESS;
	}
	
	/**
	 * 查询用户列表
	 * @param param
	 * @param session
	 * @return 
	 * @throws Exception
	 */
	public ListResult<MyUser> query(BaseQuery param, BaseSession session) throws Exception {
		Integer totalCount = userMapper.queryCount(param);
		if (totalCount <= 0) {
			throw new CustomException(ResultCode.S_NULL_ERROR);
		}
		param.setStart(PagerUtil.MathStart(param.getPagenum(), totalCount));
		List<MyUser> list = userMapper.query(param);
		return new ListResult<MyUser>(totalCount, list);
	}
	
	/**
	 * 新增用户
	 * @param user
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean add(MyUser user, BaseSession session) throws Exception {
		if(!RoleType.ADMIN.equals(session.getRoleType())){
			throw new CustomException(ResultCode.S_UNAUTH_ERROR);
		}
		if(userMapper.vaildAccount(user.getAccount())>0){
			throw new CustomException(ResultCode.ACCOUNT_EXIST);
		}
		String encodePwd = EncryptUtil.encode(user.getPassword());
		user.setPassword(encodePwd);
		return userMapper.add(user) > 0;
	}
	
	/**
	 * 编辑用户信息
	 * @param user
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean edit(MyUser user, BaseSession session) throws Exception {
		if(!RoleType.ADMIN.equals(session.getRoleType())){
			throw new CustomException(ResultCode.S_UNAUTH_ERROR);
		}
		return userMapper.edit(user) > 0;
	}
	
	/**
	 * 重置密码
	 * @param user
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean resetPwd(MyUser user, BaseSession session) throws Exception {
		if(!RoleType.ADMIN.equals(session.getRoleType())){
			throw new CustomException(ResultCode.S_UNAUTH_ERROR);
		}
		String encodePwd = EncryptUtil.encode(user.getOldPwd());
		user.setOldPwd(encodePwd);
		return userMapper.resetPwd(user) > 0;
	}

	/**
	 * 删除用户
	 * @param id
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean delete(Integer id, BaseSession session) throws Exception {
		if(!RoleType.ADMIN.equals(session.getRoleType())){
			throw new CustomException(ResultCode.S_UNAUTH_ERROR);
		}
		return userMapper.delete(id) > 0;
	}
}
