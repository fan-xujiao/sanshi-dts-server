package com.dts.server.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dts.server.common.base.BaseSession;
import com.dts.server.common.enums.ResultCode;
import com.dts.server.common.enums.RoleType;
import com.dts.server.common.exception.CustomException;
import com.dts.server.db.dao.TemplatePropMapper;
import com.dts.server.dbutil.entity.TemplateProp;

/**
 * @ClassName:  TemplatePropService
 * @author 范旭蛟
 * @date 2021年10月17日 下午9:32:30
 * @Description: 模板属性管理Service
 */
@Service
@Transactional
public class TemplatePropService {
	

	@Resource
	private TemplatePropMapper templatePropMapper;

	
	/**
	 * 查询模板属性列表
	 * @param param
	 * @param session
	 * @return 
	 * @throws Exception
	 */
	public List<TemplateProp> query(Integer templateId) throws Exception {
		return templatePropMapper.query(templateId);
	}

	/**
	 * 删除模板属性
	 * @param id
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public TemplateProp delete(Integer id, BaseSession session) throws Exception {
		if(!RoleType.ADMIN.equals(session.getRoleType())){
			throw new CustomException(ResultCode.S_UNAUTH_ERROR);
		}
		TemplateProp prop = templatePropMapper.selectById(id);
		if(templatePropMapper.delete(id) > 0){
			return prop;
		}
		return null;
	}
	
	/**
	 * 新增模板属性
	 * @param prop
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean add(TemplateProp prop, BaseSession session) throws Exception {
		if(!RoleType.ADMIN.equals(session.getRoleType())){
			throw new CustomException(ResultCode.S_UNAUTH_ERROR);
		}
		return templatePropMapper.add(prop) > 0;
	}
	
	/**
	 * 编辑模板属性信息
	 * @param prop
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean edit(TemplateProp prop, BaseSession session) throws Exception {
		if(!RoleType.ADMIN.equals(session.getRoleType())){
			throw new CustomException(ResultCode.S_UNAUTH_ERROR);
		}
		return templatePropMapper.edit(prop) > 0;
	}
}
