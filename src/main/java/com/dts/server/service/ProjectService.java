package com.dts.server.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dts.server.common.base.BaseAuthService;
import com.dts.server.common.base.BaseQuery;
import com.dts.server.common.base.BaseSession;
import com.dts.server.common.enums.ResultCode;
import com.dts.server.common.exception.CustomException;
import com.dts.server.common.result.ListResult;
import com.dts.server.common.util.PagerUtil;
import com.dts.server.db.dao.ProjectMapper;
import com.dts.server.dbutil.entity.Project;

/**
 * @ClassName:  ProjectService
 * @author 范旭蛟
 * @date 2021年10月10日 下午2:42:30
 * @Description: 项目管理service
 */
@Service
@Transactional
public class ProjectService extends BaseAuthService {
	

	@Resource
	private ProjectMapper projectMapper;
	
	/**
	 * 查询项目列表
	 * @param param
	 * @param session
	 * @return 
	 * @throws Exception
	 */
	public ListResult<Project> query(BaseQuery param, BaseSession session) throws Exception {
		Integer totalCount = projectMapper.queryCount(param, session);
		if (totalCount <= 0) {
			throw new CustomException(ResultCode.S_NULL_ERROR);
		}
		param.setStart(PagerUtil.MathStart(param.getPagenum(), totalCount));
		List<Project> list = projectMapper.query(param, session);
		return new ListResult<Project>(totalCount, list);
	}
	
	/**
	 * 新增项目
	 * @param project
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean add(Project project, BaseSession session) throws Exception {
		project.setCreaterId(session.getUserId());
		return projectMapper.add(project) > 0;
	}
	
	/**
	 * 编辑项目信息
	 * @param project
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean edit(Project project, BaseSession session) throws Exception {
		if(vaildProjectEditAuth(project.getId(), session)){
			return projectMapper.edit(project) > 0;
		}
		throw new CustomException(ResultCode.S_UNAUTH_ERROR);
	}

	/**
	 * 删除项目
	 * @param id
	 * @param session
	 * @return
	 * @throws Exception
	 */
	public boolean delete(Integer id, BaseSession session) throws Exception {
		if(vaildProjectEditAuth(id, session)){
			return projectMapper.delete(id) > 0;
		}
		throw new CustomException(ResultCode.S_UNAUTH_ERROR);
	}
}
