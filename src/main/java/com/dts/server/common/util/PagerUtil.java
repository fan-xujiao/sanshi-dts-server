package com.dts.server.common.util;

/**
 * @ClassName:  PagerUtil
 * @author 范旭蛟
 * @date 2021年10月8日 下午9:10:50
 * @Description: 分页计算工具类
 */
public class PagerUtil {

    public final static Integer PAGE_COUNT=10;
    
    /**
     * 计算起始位置
     * @param pageNum
     * @return
     */
    public static Integer MathStart(Integer pageNum){
        if(pageNum!=null && pageNum>1){
            return (pageNum-1)*PAGE_COUNT;
        }else{
            return 0;
        }
   }
   
    /**
     * 计算起始位置
     * @param pageNum
     * @param totalCount
     * @return
     */
   public static Integer MathStart(Integer pageNum, Integer totalCount){
       Integer start = MathStart(pageNum);
       Double totalPage = Math.ceil(totalCount.doubleValue() / PAGE_COUNT);
       if (pageNum > totalPage.intValue()) {
           start = MathStart(totalCount.intValue());
       }
       return start;
   }
   /**
    * 计算起始位置
    * @param pageNum 页数
    * @param count 每页条数
    * @param totalCount 总条数
    * @return
    */
   public static Integer MathStart(Integer pageNum, Integer count, Integer totalCount){
	   Integer start =0;
	   if(pageNum!=null && pageNum>1){
		   start= (pageNum-1)*count;
       }
	   Double totalPage = Math.ceil(totalCount.doubleValue() / count);
	   if (pageNum > totalPage.intValue()) {
		   start = MathStart(totalCount.intValue());
	   }
	   return start;
   }
   
   /**
    * 计算总条数
    * @param totalCount
    * @return
    */
   public static Integer MathTotalPage(Integer totalCount){
       Double totalPage = Math.ceil(totalCount.doubleValue() / PAGE_COUNT);
       return totalPage.intValue();
   }
   
   /**
    * 计算总条数
    * @param totalCount
    * @param count
    * @return
    */
   public static Integer MathTotalPage(Integer totalCount, Integer count){
	   Double totalPage = Math.ceil(totalCount.doubleValue() / count);
	   return totalPage.intValue();
   }
}
