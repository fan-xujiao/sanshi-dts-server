package com.dts.server.common.enums;

import java.io.Serializable;

import com.dts.server.common.exception.NonResultCodeException;

/**
 * @ClassName:  ResultCode
 * @author 范旭蛟
 * @date 2021年9月16日 下午9:07:10
 * @Description: 返回码枚举类型
 * 
 * 200-请求成功
 * 1001-1099：用户信息、登录异常
 * 10001-10099： 参数校验异常
 * 90001-90099：全局异常
 */
public enum ResultCode implements Serializable {
	
	SUCCESS("200", "请求成功"),
	
	//用户信息、登录异常
    PASSWORD_INVALID("1001", "用户名或密码错误"),
    ACCOUNT_NULL("1002", "用户名不能为空"),
    SESSION_INVALID("1003", "登录超时"),//登录超时
    ACCOUNT_EXIST("1004", "账号已存在"),//登录超时
    TOKEN_INVALID("1005", "无效的token"),//无效的token
    
    MONITOR_TEMPLATE_NULL("2001", "模板不能为空"),
    MONITOR_PROP_NULL("2002", "模板属性不能为空"),
    MONITOR_FLAG_INVALID("2003", "无效的监听标识"),
    
    
    //参数校验异常
    PARAM_INVALID("10001", "参数校验失败"),
    PARAM_MISTYPE("10002", "参数数据类型错误"),
    PARAM_MISSING("10003", "缺少参数"),
    PARAM_OUT_RANGE("10004", "参数超出范围"),
    PARAM_FILE_EMPTY("10005", "上传文件不能为空"),
    PARAM_FILE_TYPE_ERROR("10006","上传文件的文件格式错误"),
    PARAM_FILE_SIZE_OVERRUN("10007","上传文件的大小超过了最大限制"),
    
    //全局异常
    S_UNSUPPORTED_METHOD("90001", "不支持的请求类型"),
    S_NULL_ERROR("90002", "查询数据为空"),
    S_UNAUTH_ERROR("90003", "数据不存在或者没有访问权限"),
    S_EXCEED_ERROR("90004", "新增数据超限"),
    S_URL_NOEXIST("90006", "请求的地址不存在"),
    S_REPEAT_ERROR("90007", "该名称已存在，请勿重复添加"),
    S_UNKNOWN_ERROR("90099", "未知异常");
	
	private String code;
	private String message;
	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private ResultCode(String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public boolean codeEquals(String code){
	    if(this.code.equals(code)){
	        return true;
	    }
	    return false;
	}
    /**
     * 根据code获取去value
     * @param code
     * @return
     */
    public static ResultCode getValueByCode(String code){
        for(ResultCode resultCode:ResultCode.values()){
            if(code.equals(resultCode.getCode())){
                return resultCode;
            }
        }
        //没有对应的resultCode，抛出异常
        throw new NonResultCodeException(code);
    }
	
    /**
     * 根据code获取去resultcode
     * @param code
     * @return
     */
    public static ResultCode getResultByCode(String code){
    	for(ResultCode resultCode:ResultCode.values()){
    		if(code.equals(resultCode.getCode())){
    			return resultCode;
    		}
    	}
    	//没有对应的resultCode，抛出异常
    	return null;
    }
    
    
}
