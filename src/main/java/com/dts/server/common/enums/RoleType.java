package com.dts.server.common.enums;


public enum RoleType{
    //超级管理员
    ADMIN("admin"),
    //普通账号
    USER("user");
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private RoleType(String value) {
		this.value = value;
	}
	
	/**
	 * 判断是否相等
	 * @param roleType
	 * @return
	 */
	public boolean equals(String roleType){
		return value.equals(roleType);
	}
}
