package com.dts.server.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ClassName:  Authority
 * @author 范旭蛟
 * @date 2021年9月16日 下午8:48:34
 * @Description: 访问接口需要的权限控制时使用，多个权限间用逗号隔开
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Authority {
	
	 String value() default "";
}
