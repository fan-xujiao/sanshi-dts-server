package com.dts.server.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ClassName:  Expose
 * @author 范旭蛟
 * @date 2021年9月16日 下午8:47:45
 * @Description: 添加该注释的接口可以免登录访问
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Expose {}
