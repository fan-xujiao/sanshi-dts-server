package com.dts.server.common.base;

import java.util.List;

import javax.servlet.http.HttpSession;

/**
 * @ClassName:  BaseSession
 * @author 范旭蛟
 * @date 2021年10月8日 下午9:05:52
 * @Description: 用于将httpsession对象转换
 */
public class BaseSession {
    
	private Integer userId;
	private String account;
	private String roleType;
	private String fullName;
	private String department;
	private String title;
	private String telphone;
	private String email;
    private List<String> auths;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<String> getAuths() {
		return auths;
	}

	public void setAuths(List<String> auths) {
		this.auths = auths;
	}

	@SuppressWarnings("unchecked")
    public BaseSession(HttpSession session) {
        super();
        this.userId = (Integer)session.getAttribute("userId");
        this.account = (String)session.getAttribute("account");
        this.roleType = (String)session.getAttribute("roleType");
        this.fullName = (String)session.getAttribute("fullName");
        this.department = (String)session.getAttribute("department");
        this.title = (String)session.getAttribute("title");
        this.telphone = (String)session.getAttribute("telphone");
        this.email = (String)session.getAttribute("email");
        this.auths = (List<String>)session.getAttribute("auths");
   }
}
