package com.dts.server.common.base;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dts.server.db.dao.ProjectAuthMapper;
import com.dts.server.db.dao.ProjectMapper;
import com.dts.server.dbutil.entity.ProjectAuth;
import com.dts.server.dbutil.entity.ProjectAuthKey;

/**
 * @ClassName:  BaseAuthService
 * @author 范旭蛟
 * @date 2021年10月10日 下午3:00:31
 * @Description: Service权限验证基类
 */
@Service
@Transactional
public class BaseAuthService {
	
	@Resource
	private ProjectMapper projectMapper;
	
	@Resource
	private ProjectAuthMapper projectAuthMapper;
	
	/**
	 * 验证是否有项目查看权限
	 * @param projectId
	 * @param session
	 * @return
	 * @throws Exception
	 */
	protected boolean vaildProjectAuth(Integer projectId, BaseSession session) throws Exception{
		if("admin".equals(session.getRoleType())){
			return true;
		}
		if(projectMapper.isCreater(projectId, session.getUserId())){
			return true;
		}
		ProjectAuth auth = projectAuthMapper.selectById(new ProjectAuthKey(projectId, session.getUserId()));
		if(auth != null){
			return true;
		}
		return false;
	}
	
	/**
	 * 验证是否有项目编辑权限
	 * @param id
	 * @param session
	 * @return
	 */
	protected boolean vaildProjectEditAuth(Integer projectId, BaseSession session) throws Exception{
		if("admin".equals(session.getRoleType())){
			return true;
		}
		if(projectMapper.isCreater(projectId, session.getUserId())){
			return true;
		}
		ProjectAuth auth = projectAuthMapper.selectById(new ProjectAuthKey(projectId, session.getUserId()));
		if(auth != null && auth.getIsEdit()){
			return true;
		}
		return false;
	}
}
