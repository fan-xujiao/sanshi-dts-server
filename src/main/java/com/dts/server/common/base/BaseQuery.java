package com.dts.server.common.base;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Range;

/**
 * @ClassName:  BaseQuery
 * @author 范旭蛟
 * @date 2021年10月8日 下午9:05:16
 * @Description: 查询pojo基类
 */
public class BaseQuery {

	@Min(value = 1,message = "页码必须大于0")
	private Integer pagenum=1;

    @Range(min = 1, max = 10000, message = "一次性获取最大列表数不能小于1且不能超过1000")
    private Integer count=10;
    
    private Integer start;
    
    //查询参数
    private String queryParam;
    
	public Integer getPagenum() {
		return pagenum==null?1:pagenum;
	}

	public void setPagenum(Integer pagenum) {
		this.pagenum = pagenum;
	}

	public Integer getCount() {
		return count==null?10:count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

    public String getQueryParam() {
        return queryParam;
    }

    public void setQueryParam(String queryParam) {
        this.queryParam = queryParam == null ? null : queryParam.trim();
    }

}
