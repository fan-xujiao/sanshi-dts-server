package com.dts.server.common.result;

import java.util.List;

import com.dts.server.common.util.PagerUtil;

/**
 * @ClassName:  ListResult
 * @author 范旭蛟
 * @date 2021年10月8日 下午9:11:51
 * @Description: 列表查询接口返回结果
 * @param <T>
 */
public class ListResult<T> {

    private Integer totalPage;
    private Integer totalCount;
    private List<T> list;
    
    public Integer getTotalPage() {
        return totalPage;
    }
    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }
    public Integer getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }
    public List<T> getList() {
        return list;
    }
    public void setList(List<T> list) {
        this.list = list;
    }
    
    public ListResult() {
        super();
    }
    
    public ListResult(Integer totalCount, List<T> list) {
        super();
        this.totalPage = PagerUtil.MathTotalPage(totalCount);
        this.totalCount = totalCount;
        this.list = list;
    }
    public ListResult(Integer totalCount, Integer count, List<T> list) {
    	super();
    	this.totalPage = PagerUtil.MathTotalPage(totalCount,count);
    	this.totalCount = totalCount;
    	this.list = list;
    }
}
