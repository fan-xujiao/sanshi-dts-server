package com.dts.server.common.result;

import java.io.Serializable;

import com.dts.server.common.enums.ResultCode;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @ClassName:  HttpResult
 * @author 范旭蛟
 * @date 2021年10月8日 下午9:12:32
 * @Description: http接口返回结果
 * @param <T>
 */
public class HttpResult<T> implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -1880638067927621694L;
	
    private String code;
    private String message;
    @JsonInclude(Include.NON_NULL)
    private T data;

    private HttpResult(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    private HttpResult(ResultCode resultCode, T data) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
        this.data = data;
    }

    public HttpResult() {
		// TODO Auto-generated constructor stub
	}

	/**
     * 成功返回
     */
    public static <T> HttpResult<T> ok(T data) {
        return new HttpResult<>(ResultCode.SUCCESS, data);
    }
    
    /**
     * 成功返回-自定义消息内容
     */
    public static <T> HttpResult<T> ok(T data, String msg) {
        return new HttpResult<>(ResultCode.SUCCESS.getCode(), msg, data);
    }

    /**
     * 异常返回-指定错误码
     */
    public static HttpResult<Object> fail(ResultCode resultCode) {
        return new HttpResult<>(resultCode, null);
    }

    /**
     * 异常返回-非指定异常
     */
    public static HttpResult<Object> fail(String code, String message) {
        return new HttpResult<>(code, message, null);
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
