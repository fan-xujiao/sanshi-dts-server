package com.dts.server.common.exception;

public class NonResultCodeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4457533885197142111L;
	
    //错误信息
    private String message;
    //错误代码
    private String code;

    /**
     * 构造函数
     * @param resultCode 错误码
     */
    public NonResultCodeException(String code){
        this.code = code;
    }

    
    /**
     * 构造函数
     * @param resultCode
     * @param message
     */
    public NonResultCodeException(String code, String message){
    	this.code = code;
    	this.message = message;
    }


    /**
     * 获取错误信息
     */
	public String getMessage() {
		return message;
	}

	public String getCode() {
		return code;
	}

    
	
	
}
