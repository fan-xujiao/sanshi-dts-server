package com.dts.server.common.exception;

import java.text.MessageFormat;

import com.dts.server.common.enums.ResultCode;

public class CustomException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4457533885197142111L;
	
	//错误代码
    private ResultCode resultCode;
    //错误信息
    private String message;

    /**
     * 构造函数
     * @param resultCode 错误码
     */
    public CustomException(ResultCode resultCode){
        this.resultCode = resultCode;
        this.message = resultCode.getMessage();
    }

    /**
     * 构造函数
     * @param resultCode
     * @param args
     */
    public CustomException(ResultCode resultCode, Object... args){
        this.resultCode = resultCode;
        this.message = MessageFormat.format(resultCode.getMessage(), args);
    }
    
    /**
     * 构造函数
     * @param resultCode
     * @param message
     */
    public CustomException(ResultCode resultCode, String message){
    	this.resultCode = resultCode;
    	this.message = message;
    }

    /**
     * 获取错误码
     * @return
     */
    public ResultCode getResultCode(){
        return resultCode;
    }

    /**
     * 获取错误信息
     */
	public String getMessage() {
		return message;
	}
    
}
