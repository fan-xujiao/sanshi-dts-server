package com.dts.server.query;

import com.dts.server.common.util.EncryptUtil;

/**
 * @ClassName:  LoginQuery
 * @author 范旭蛟
 * @date 2021年9月16日 下午9:13:53
 * @Description: 登录请求参数
 */
public class LoginQuery {

    private String account;
    private String password;
    
    public String getAccount() {
		return account;
	}
    
	public void setAccount(String account) {
		this.account = account == null ? null : account.trim();
	}
	
	public String getPassword() {
		//将密码加密
        return EncryptUtil.encode(password);
    }
	
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }
    
}
