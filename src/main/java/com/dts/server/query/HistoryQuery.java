package com.dts.server.query;

import java.util.Date;

import com.dts.server.common.base.BaseQuery;

public class HistoryQuery extends BaseQuery {

	//监听id
	private Integer monitorId;
	//起始时间
	private Date from;
	//结束时间
	private Date end;
	
	public Integer getMonitorId() {
		return monitorId;
	}

	public void setMonitorId(Integer monitorId) {
		this.monitorId = monitorId;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}
	
}
