package com.dts.server.query;

import com.dts.server.common.base.BaseQuery;

public class TemplateTriggerQuery extends BaseQuery {
	
	//模板id
	private Integer templateId;
	//属性key
	private String propKey;
	
	public Integer getTemplateId() {
		return templateId;
	}
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}
	public String getPropKey() {
		return propKey;
	}
	public void setPropKey(String propKey) {
		this.propKey = propKey;
	}
	
	
	
}
