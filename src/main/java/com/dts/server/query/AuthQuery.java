package com.dts.server.query;

import com.dts.server.common.base.BaseQuery;

public class AuthQuery extends BaseQuery {

	private Integer projectId;

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	
	
}
