package com.dts.server.query;

import com.dts.server.common.base.BaseQuery;

public class ProjectMonitorQuery extends BaseQuery {

	private Integer projectId;
	
	private String flag;

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	
}
