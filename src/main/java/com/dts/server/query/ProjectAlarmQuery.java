package com.dts.server.query;

import java.util.Date;

import com.dts.server.common.base.BaseQuery;

/**
 * @ClassName:  AlarmLogQuery
 * @author 范旭蛟
 * @date 2021年10月8日 下午9:18:08
 * @Description: 报警日志查询参数类
 */
public class ProjectAlarmQuery extends BaseQuery {

	//项目id
	private Integer projectId;
	//设备类型
	private String equipmentType;
	//设备id
	private Integer equipmentId;
	//起始时间
	private Date from;
	//结束时间
	private Date end;

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getEquipmentType() {
		return equipmentType;
	}

	public void setEquipmentType(String equipmentType) {
		this.equipmentType = equipmentType;
	}

	public Integer getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(Integer equipmentId) {
		this.equipmentId = equipmentId;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}
	
	
}
