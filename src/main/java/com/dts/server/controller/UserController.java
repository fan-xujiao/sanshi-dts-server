package com.dts.server.controller;

import java.util.Enumeration;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dts.server.common.annotation.Expose;
import com.dts.server.common.base.BaseController;
import com.dts.server.common.base.BaseQuery;
import com.dts.server.common.base.BaseSession;
import com.dts.server.common.enums.ResultCode;
import com.dts.server.common.result.HttpResult;
import com.dts.server.common.result.ListResult;
import com.dts.server.db.entity.MyUser;
import com.dts.server.query.LoginQuery;
import com.dts.server.service.UserService;

/**
 * @ClassName:  UserController
 * @author 范旭蛟
 * @date 2021年9月16日 下午9:17:41
 * @Description: 用户管理controller
 */
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

	@Resource
	private UserService userService;
	
	/**
	 * 登录
	 * @param param
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@Expose
	@RequestMapping("/login")
	public HttpResult<?> login(@Validated LoginQuery param, HttpSession session) throws Exception{
	    ResultCode resultCode = userService.login(param, session);
	    if(ResultCode.SUCCESS.equals(resultCode)){
	        return responseOK();
        }else{
            return responseFail(resultCode);
        }
	}
	
	/**
	 * 获取授权信息
	 * @param param
	 * @param session
	 * @return
	 * @throws Exception
	 */
    @RequestMapping("/info")
	public HttpResult<?> info(HttpSession session) throws Exception{
	    BaseSession bsession = new BaseSession(session);
	    if(StringUtils.isNotBlank(bsession.getAccount())){
	        return responseOK(bsession);
	    }
	    return responseFail(ResultCode.S_NULL_ERROR);
	}
	
	/**
	 * 注销
	 * @param param
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/logout")
    public HttpResult<?> logout(HttpSession session) throws Exception{
	    Enumeration<?> em = session.getAttributeNames();
        while(em.hasMoreElements()){
            String sessionName=em.nextElement().toString();
            session.removeAttribute(sessionName);
        }
	    return responseOK();
    }
	
	/**
	 * 查询
	 * 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/query")
	public HttpResult<?> query(@Validated BaseQuery param, HttpSession session) throws Exception {
		ListResult<MyUser> result = userService.query(param, new BaseSession(session));
		if (result != null) {
			return responseOK(result);
		}
		return responseFail(ResultCode.S_NULL_ERROR);
	}

	/**
	 * 新增用户
	 * @param user
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/add")
	public HttpResult<?> add(MyUser user, HttpSession session) throws Exception {
		if (userService.add(user, new BaseSession(session))) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}

	/**
	 * 编辑用户信息
	 * @param user
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/edit")
	public HttpResult<?> edit(MyUser user, HttpSession session) throws Exception {
		if (userService.edit(user, new BaseSession(session))) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}
	
	/**
	 * 重置用户密码
	 * @param user
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/resetPwd")
	public HttpResult<?> resetPwd(MyUser user, HttpSession session) throws Exception {
		if (userService.resetPwd(user, new BaseSession(session))) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}

	/**
	 * 删除用户
	 * @param id
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/delete")
	public HttpResult<?> delete(Integer id, HttpSession session) throws Exception {
		if (userService.delete(id, new BaseSession(session))) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}
}
