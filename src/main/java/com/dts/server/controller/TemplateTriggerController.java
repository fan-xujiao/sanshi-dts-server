package com.dts.server.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dts.server.common.base.BaseController;
import com.dts.server.common.base.BaseSession;
import com.dts.server.common.enums.ResultCode;
import com.dts.server.common.result.HttpResult;
import com.dts.server.dbutil.entity.TemplateTrigger;
import com.dts.server.service.TemplateTriggerService;

/**
 * @ClassName:  TemplateController 
 * @author 范旭蛟
 * @date 2021年10月25日 下午8:13:26
 * @Description: 模板触发器 Controller
 */
@RestController
@RequestMapping("/template/trigger")
public class TemplateTriggerController extends BaseController {

	@Resource
	private TemplateTriggerService templateTriggerService;
	
	/**
	 * 查询
	 * @param templateId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/query")
	public HttpResult<?> query(Integer templateId, HttpSession session) throws Exception {
		List<TemplateTrigger> result = templateTriggerService.query(templateId, new BaseSession(session));
		if (result != null) {
			return responseOK(result);
		}
		return responseFail(ResultCode.S_NULL_ERROR);
	}

	/**
	 * 新增模板触发器
	 * @param trigger
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/add")
	public HttpResult<?> add(TemplateTrigger trigger, HttpSession session) throws Exception {
		if (templateTriggerService.add(trigger, new BaseSession(session))) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}

	/**
	 * 编辑模板触发器
	 * @param trigger
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/edit")
	public HttpResult<?> edit(TemplateTrigger trigger, HttpSession session) throws Exception {
		if (templateTriggerService.edit(trigger, new BaseSession(session))) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}

	/**
	 * 删除模板触发器
	 * @param id
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/delete")
	public HttpResult<?> delete(Integer id, HttpSession session) throws Exception {
		if (templateTriggerService.delete(id, new BaseSession(session))) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}
}
