package com.dts.server.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dts.server.common.base.BaseController;
import com.dts.server.common.base.BaseSession;
import com.dts.server.common.enums.ResultCode;
import com.dts.server.common.result.HttpResult;
import com.dts.server.db.entity.MyTemplateProp;
import com.dts.server.dbutil.entity.ProjectMonitor;
import com.dts.server.dbutil.entity.TemplateProp;
import com.dts.server.service.HistoryTableService;
import com.dts.server.service.ProjectMonitorService;
import com.dts.server.service.TemplatePropService;

/**
 * @ClassName:  TemplateController 
 * @author 范旭蛟
 * @date 2021年10月25日 下午8:13:26
 * @Description: 模板属性 Controller
 */
@RestController
@RequestMapping("/template/prop")
public class TemplatePropController extends BaseController {

	@Resource
	private TemplatePropService templatePropService;
	
	@Resource
	private ProjectMonitorService monitorService;
	
	@Resource
	private HistoryTableService tableService;
	
	/**
	 * 查询
	 * @param templateId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/query")
	public HttpResult<?> query(Integer templateId, HttpSession session) throws Exception {
		List<TemplateProp> result = templatePropService.query(templateId);
		if (result != null) {
			return responseOK(result);
		}
		return responseFail(ResultCode.S_NULL_ERROR);
	}

	/**
	 * 新增模板属性
	 * @param prop
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/add")
	public HttpResult<?> add(TemplateProp prop, HttpSession session) throws Exception {
		if (templatePropService.add(prop, new BaseSession(session))) {
			List<ProjectMonitor> list = monitorService.queryByTemplate(prop.getTemplateId());
			if(list != null){
				for(ProjectMonitor monitor : list){
					tableService.addColumn(monitor.getId(), prop);
				}
			}
			
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}

	/**
	 * 编辑模板属性
	 * @param prop
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/edit")
	public HttpResult<?> edit(MyTemplateProp prop, HttpSession session) throws Exception {
		if (templatePropService.edit(prop, new BaseSession(session))) {
			List<ProjectMonitor> list = monitorService.queryByTemplate(prop.getTemplateId());
			if(list != null){
				for(ProjectMonitor monitor : list){
					tableService.editColumn(monitor.getId(), prop);
				}
			}
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}

	/**
	 * 删除模板属性
	 * @param id
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/delete")
	public HttpResult<?> delete(Integer id, HttpSession session) throws Exception {
		TemplateProp prop = templatePropService.delete(id, new BaseSession(session));
		if (prop != null && prop.getTemplateId() != null) {
			List<ProjectMonitor> list = monitorService.queryByTemplate(prop.getTemplateId());
			if(list != null){
				for(ProjectMonitor monitor : list){
					tableService.dropColumn(monitor.getId(), prop.getPropKey());
				}
			}
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}
}
