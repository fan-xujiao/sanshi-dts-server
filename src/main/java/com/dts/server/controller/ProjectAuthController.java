package com.dts.server.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dts.server.common.base.BaseController;
import com.dts.server.common.base.BaseSession;
import com.dts.server.common.enums.ResultCode;
import com.dts.server.common.result.HttpResult;
import com.dts.server.common.result.ListResult;
import com.dts.server.dbutil.entity.ProjectAuth;
import com.dts.server.query.AuthQuery;
import com.dts.server.service.ProjectAuthService;

/**
 * @ClassName:  TemplateController 
 * @author 范旭蛟
 * @date 2021年10月25日 下午8:13:26
 * @Description: 项目授权 Controller
 */
@RestController
@RequestMapping("/project/auth")
public class ProjectAuthController extends BaseController {

	@Resource
	private ProjectAuthService projectAuthService;
	
	/**
	 * 查询
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/query")
	public HttpResult<?> query(AuthQuery param, HttpSession session) throws Exception {
		ListResult<ProjectAuth> result = projectAuthService.query(param, new BaseSession(session));
		if (result != null) {
			return responseOK(result);
		}
		return responseFail(ResultCode.S_NULL_ERROR);
	}

	/**
	 * 新增项目授权
	 * @param auth
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/add")
	public HttpResult<?> add(ProjectAuth auth, HttpSession session) throws Exception {
		if (projectAuthService.add(auth, new BaseSession(session))) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}

	/**
	 * 编辑项目授权
	 * @param auth
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/edit")
	public HttpResult<?> edit(ProjectAuth auth, HttpSession session) throws Exception {
		if (projectAuthService.edit(auth, new BaseSession(session))) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}

	/**
	 * 删除项目授权
	 * @param auth
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/delete")
	public HttpResult<?> delete(ProjectAuth auth, HttpSession session) throws Exception {
		if (projectAuthService.delete(auth, new BaseSession(session))) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}
}
