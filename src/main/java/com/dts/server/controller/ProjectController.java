package com.dts.server.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dts.server.common.base.BaseController;
import com.dts.server.common.base.BaseQuery;
import com.dts.server.common.base.BaseSession;
import com.dts.server.common.enums.ResultCode;
import com.dts.server.common.result.HttpResult;
import com.dts.server.common.result.ListResult;
import com.dts.server.dbutil.entity.Project;
import com.dts.server.service.ProjectService;

/**
 * @ClassName:  ProjectController 
 * @author 范旭蛟
 * @date 2021年10月25日 下午8:13:26
 * @Description: 项目 Controller
 */
@RestController
@RequestMapping("/project")
public class ProjectController extends BaseController {

	@Resource
	private ProjectService projectService;
	
	/**
	 * 查询
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/query")
	public HttpResult<?> query(@Validated BaseQuery param, HttpSession session) throws Exception {
		ListResult<Project> result = projectService.query(param, new BaseSession(session));
		if (result != null) {
			return responseOK(result);
		}
		return responseFail(ResultCode.S_NULL_ERROR);
	}

	/**
	 * 新增项目
	 * @param project
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/add")
	public HttpResult<?> add(Project project, HttpSession session) throws Exception {
		if (projectService.add(project, new BaseSession(session))) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}

	/**
	 * 编辑项目
	 * @param project
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/edit")
	public HttpResult<?> edit(Project project, HttpSession session) throws Exception {
		if (projectService.edit(project, new BaseSession(session))) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}

	/**
	 * 删除项目
	 * @param id
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/delete")
	public HttpResult<?> delete(Integer id, HttpSession session) throws Exception {
		if (projectService.delete(id, new BaseSession(session))) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}
}
