package com.dts.server.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dts.server.common.base.BaseController;
import com.dts.server.common.base.BaseSession;
import com.dts.server.common.enums.ResultCode;
import com.dts.server.common.result.HttpResult;
import com.dts.server.common.result.ListResult;
import com.dts.server.dbutil.entity.ProjectMonitor;
import com.dts.server.dbutil.entity.TemplateProp;
import com.dts.server.query.ProjectMonitorQuery;
import com.dts.server.service.HistoryTableService;
import com.dts.server.service.ProjectMonitorService;
import com.dts.server.service.TemplatePropService;

/**
 * @ClassName:  TemplateController 
 * @author 范旭蛟
 * @date 2021年10月25日 下午8:13:26
 * @Description: 项目监听 Controller
 */
@RestController
@RequestMapping("/project/monitor")
public class ProjectMonitorController extends BaseController {

	@Resource
	private ProjectMonitorService projectMonitorService;
	
	private TemplatePropService templatePropService;
	
	private HistoryTableService historyTableService;
	
	/**
	 * 查询
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/query")
	public HttpResult<?> query(ProjectMonitorQuery param, HttpSession session) throws Exception {
		ListResult<ProjectMonitor> result = projectMonitorService.query(param, new BaseSession(session));
		if (result != null) {
			return responseOK(result);
		}
		return responseFail(ResultCode.S_NULL_ERROR);
	}

	/**
	 * 新增项目监听
	 * @param monitor
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/add")
	public HttpResult<?> add(ProjectMonitor monitor, HttpSession session) throws Exception {
		if (projectMonitorService.add(monitor, new BaseSession(session))) {
			if(monitor.getTemplateId()==null){
				return responseFail(ResultCode.MONITOR_TEMPLATE_NULL);
			}
			List<TemplateProp> props = templatePropService.query(monitor.getTemplateId());
			if(props == null || props.size() == 0){
				return responseFail(ResultCode.MONITOR_PROP_NULL);
			}
			if(historyTableService.createTable(monitor.getId(), props)){
				return responseOK();
			}
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}

	/**
	 * 编辑项目监听
	 * @param monitor
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/edit")
	public HttpResult<?> edit(ProjectMonitor monitor, HttpSession session) throws Exception {
		if (projectMonitorService.edit(monitor, new BaseSession(session))) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}

	/**
	 * 删除项目监听
	 * @param monitor
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/delete")
	public HttpResult<?> delete(ProjectMonitor monitor, HttpSession session) throws Exception {
		if (projectMonitorService.delete(monitor, new BaseSession(session))) {
			historyTableService.dropTable(monitor.getId());
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}
}
