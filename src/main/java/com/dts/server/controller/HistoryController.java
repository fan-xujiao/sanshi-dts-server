package com.dts.server.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dts.server.common.base.BaseController;
import com.dts.server.common.base.BaseSession;
import com.dts.server.common.enums.ResultCode;
import com.dts.server.common.result.HttpResult;
import com.dts.server.common.result.ListResult;
import com.dts.server.query.HistoryQuery;
import com.dts.server.service.HistoryService;
import com.dts.server.service.ProjectAuthService;

/**
 * @ClassName:  HistoryController 
 * @author 范旭蛟
 * @date 2021年10月25日 下午8:13:26
 * @Description: 项目 Controller
 */
@RestController
@RequestMapping("/history")
public class HistoryController extends BaseController {

	@Resource
	private HistoryService historyService;
	
	@Resource
	private ProjectAuthService projectAuthService;
	
	/**
	 * 分页查询历史数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/query")
	public HttpResult<?> query(@Validated HistoryQuery param, HttpSession session) throws Exception {
		if(!projectAuthService.vaildMonitorAuth(param.getMonitorId(), new BaseSession(session))){
			return responseFail(ResultCode.S_UNAUTH_ERROR);
		}
		ListResult<Map<String,Object>> result = historyService.query(param);
		if (result != null) {
			return responseOK(result);
		}
		return responseFail(ResultCode.S_NULL_ERROR);
	}
	
	/**
	 * 查询所有历史数据
	 * @param param
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/queryAll")
	public HttpResult<?> queryAll(@Validated HistoryQuery param, HttpSession session) throws Exception {
		if(!projectAuthService.vaildMonitorAuth(param.getMonitorId(), new BaseSession(session))){
			return responseFail(ResultCode.S_UNAUTH_ERROR);
		}
		List<Map<String,Object>> result = historyService.queryAll(param);
		if (result != null) {
			return responseOK(result);
		}
		return responseFail(ResultCode.S_NULL_ERROR);
	}

	/**
	 * 清空历史数据
	 * @param param
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/clear")
	public HttpResult<?> clear(@Validated HistoryQuery param, HttpSession session) throws Exception {
		if(!projectAuthService.vaildMonitorAuth(param.getMonitorId(), new BaseSession(session))){
			return responseFail(ResultCode.S_UNAUTH_ERROR);
		}
		if (historyService.clear(param)) {
			return responseOK();
		}
		return responseFail(ResultCode.S_UNKNOWN_ERROR);
	}
}
