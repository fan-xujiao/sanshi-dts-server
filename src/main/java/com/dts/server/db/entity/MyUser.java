package com.dts.server.db.entity;

import com.dts.server.dbutil.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class MyUser extends User {

    /**
     * 
     */
    private static final long serialVersionUID = -2167041934072877529L;
    
    @JsonIgnore
    private String oldPwd;

	public String getOldPwd() {
		return oldPwd;
	}

	public void setOldPwd(String oldPwd) {
		this.oldPwd = oldPwd;
	}
    
	
}
