package com.dts.server.db.entity;

import com.dts.server.dbutil.entity.TemplateProp;

public class MyTemplateProp extends TemplateProp {

	private String oldKey;

	public String getOldKey() {
		return oldKey;
	}

	public void setOldKey(String oldKey) {
		this.oldKey = oldKey;
	}
	
}
