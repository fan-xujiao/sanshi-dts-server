package com.dts.server.db.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.dts.server.dbutil.entity.ProjectMonitor;
import com.dts.server.query.ProjectMonitorQuery;

/**
 * @ClassName:  ProjectMonitorMapper
 * @author 范旭蛟
 * @date 2021年10月8日 下午9:28:31
 * @Description: 监控sql映射接口
 */
@Component
public interface ProjectMonitorMapper {
    
    /**
     * 查询监控列表
     * @param param
     * @return
     * @throws Exception
     */
    public List<ProjectMonitor> query(ProjectMonitorQuery param) throws Exception;
    
    
    /**
     * 查询监控总数
     * @param param
     * @return
     * @throws Exception
     */
    public Integer queryCount(ProjectMonitorQuery param) throws Exception;
    
    /**
     * 根据模板id查询所有的监听
     * @param templateId
     * @return
     * @throws Exception
     */
    public List<ProjectMonitor> queryByTemplate(Integer templateId) throws Exception;
    
    /**
     * 查询监控详情
     * @param id
     * @return
     * @throws Exception
     */
    public ProjectMonitor selectById(Integer id) throws Exception;
    
    /**
     * 根据标识查询监控详情
     * @param flag
     * @return
     * @throws Exception
     */
    public ProjectMonitor selectByFlag(String flag) throws Exception;
    
    /**
     * 新增监控
     * @param host
     * @throws Exception
     */
    public int add(ProjectMonitor monitor) throws Exception;
    
    /**
     * 编辑监控信息
     * @param host
     * @throws Exception
     */
    public int edit(ProjectMonitor monitor) throws Exception;
    
    /**
     * 删除监控
     * @param host
     * @return
     * @throws Exception
     */
    public int delete(ProjectMonitor monitor) throws Exception;
}
