package com.dts.server.db.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.dts.server.common.base.BaseQuery;
import com.dts.server.dbutil.entity.Template;

/**
 * @ClassName:  TemplateMapper
 * @author 范旭蛟
 * @date 2021年10月17日 下午4:19:20
 * @Description: 监听模板sql映射接口
 */
@Component
public interface TemplateMapper {
    
    /**
     * 分页查询
     * @param param
     * @return
     * @throws Exception
     */
    public List<Template> query(BaseQuery param) throws Exception;
    
    /**
     * 查询总数
     * @param param
     * @return
     * @throws Exception
     */
    public Integer queryCount(BaseQuery param) throws Exception;
    
    /**
     * 删除
     * @param id
     * @return
     * @throws Exception
     */
    public int delete(Integer id) throws Exception;
    
    /**
     * 新增
     * @param template
     * @throws Exception
     */
    public int add(Template template) throws Exception;
    
    /**
     * 编辑
     * @param template
     * @throws Exception
     */
    public int edit(Template template) throws Exception;
}
