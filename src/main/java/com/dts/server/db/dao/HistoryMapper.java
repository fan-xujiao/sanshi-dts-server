package com.dts.server.db.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.dts.server.dbutil.entity.MonitorData;
import com.dts.server.query.HistoryQuery;

/**
 * @ClassName:  HistoryMapper
 * @author 范旭蛟
 * @date 2021年10月2日 下午8:42:42
 * @Description: 历史数据sql映射接口
 */
@Component
public interface HistoryMapper {
    
    /**
     * 分页查询历史数据列表
     * @param param
     * @return
     * @throws Exception
     */
    public List<Map<String,Object>> query(HistoryQuery param) throws Exception;
    
    /**
     * 查询历史数据总条数
     * @param param
     * @return
     * @throws Exception
     */
    public Integer queryCount(HistoryQuery param) throws Exception;
    
    /**
     * 查询符合条件的所有历史数据
     * @param param
     * @return
     * @throws Exception
     */
    public List<Map<String,Object>> queryAll(HistoryQuery param) throws Exception;
    
    /**
     * 清除历史数据
     * @param param
     * @return
     * @throws Exception
     */
    public int clear(HistoryQuery param) throws Exception;
        
    /**
     * 追加历史数据
     * @param log
     * @return
     * @throws Exception
     */
    public int append(MonitorData data) throws Exception;
}
