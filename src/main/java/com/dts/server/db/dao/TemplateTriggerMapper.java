package com.dts.server.db.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.dts.server.dbutil.entity.TemplateTrigger;

/**
 * @ClassName:  TemplateTriggerMapper
 * @author 范旭蛟
 * @date 2021年10月8日 下午9:28:31
 * @Description: 监听模板触发器sql映射接口
 */
@Component
public interface TemplateTriggerMapper {
    
	/**
     * 查询
     * @param param
     * @return
     * @throws Exception
     */
    public List<TemplateTrigger> query(Integer templateId) throws Exception;
    
    /**
     * 删除
     * @param id
     * @return
     * @throws Exception
     */
    public int delete(Integer id) throws Exception;
    
    /**
     * 新增
     * @param trigger
     * @throws Exception
     */
    public int add(TemplateTrigger trigger) throws Exception;
    
    /**
     * 编辑
     * @param trigger
     * @throws Exception
     */
    public int edit(TemplateTrigger trigger) throws Exception;
}
