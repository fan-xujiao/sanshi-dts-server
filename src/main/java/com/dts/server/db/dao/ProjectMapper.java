package com.dts.server.db.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.dts.server.common.base.BaseQuery;
import com.dts.server.common.base.BaseSession;
import com.dts.server.dbutil.entity.Project;

/**
 * @ClassName:  ProjectMapper
 * @author 范旭蛟
 * @date 2021年9月27日 下午9:05:55
 * @Description: 项目管理 sql映射
 */
@Component
public interface ProjectMapper {
    
    /**
     * 查询项目列表
     * @param param
     * @return
     * @throws Exception
     */
    public List<Project> query(@Param("param")BaseQuery param, @Param("session")BaseSession session) throws Exception;
    
    /**
     * 查询项目总数
     * @param param
     * @return
     * @throws Exception
     */
    public Integer queryCount(@Param("param")BaseQuery param, @Param("session")BaseSession session) throws Exception;
    
    /**
     * 验证
     * @param projectId
     * @param userId
     * @return
     * @throws Exception
     */
    public Boolean isCreater(@Param("projectId")Integer projectId, @Param("userId")Integer userId) throws Exception;
    
    /**
     * 新增项目
     * @param Project
     * @throws Exception
     */
    public int add(Project Project) throws Exception;
    
    /**
     * 编辑项目信息
     * @param Project
     * @throws Exception
     */
    public int edit(Project Project) throws Exception;
    
    /**
     * 编辑拓扑图文件地址
     * @param Project
     * @return
     * @throws Exception
     */
    public int editTopology(Project Project) throws Exception;
    
    /**
     * 删除项目
     * @param id
     * @return
     * @throws Exception
     */
    public int delete(Integer id) throws Exception;
}
