package com.dts.server.db.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.dts.server.common.base.BaseQuery;
import com.dts.server.db.entity.MyUser;
import com.dts.server.query.LoginQuery;

/**
 * @ClassName:  UserMapper
 * @author 范旭蛟
 * @date 2021年9月16日 下午9:19:07
 * @Description: 用户管理 sql映射
 */
@Component
public interface UserMapper {

    /**
     * 验证登录用户
     * @param param
     * @return
     * @throws Exception
     */
    public MyUser login(LoginQuery param) throws Exception;
    
    /**
     * 查询用户列表
     * @param param
     * @return
     * @throws Exception
     */
    public List<MyUser> query(BaseQuery param) throws Exception;
    
    /**
     * 查询用户总数
     * @param param
     * @return
     * @throws Exception
     */
    public Integer queryCount(BaseQuery param) throws Exception;
    
    /**
     * 验证账号是否存在
     * @param account
     * @return
     * @throws Exception
     */
    public Integer vaildAccount(String account) throws Exception;
    
    /**
     * 新增用户
     * @param user
     * @throws Exception
     */
    public int add(MyUser user) throws Exception;
    
    /**
     * 编辑用户信息
     * @param user
     * @throws Exception
     */
    public int edit(MyUser user) throws Exception;
    
    /**
     * 修改用户密码
     * @param user
     * @return
     * @throws Exception
     */
    public int resetPwd(MyUser user) throws Exception;
    
    /**
     * 删除用户
     * @param id
     * @return
     * @throws Exception
     */
    public int delete(Integer id) throws Exception;
}
