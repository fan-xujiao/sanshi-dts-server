package com.dts.server.db.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.dts.server.dbutil.entity.ProjectAlarm;
import com.dts.server.query.ProjectAlarmQuery;

/**
 * @ClassName:  ProjectAuthMapper
 * @author 范旭蛟
 * @date 2021年10月2日 下午8:42:42
 * @Description: 报警日志sql映射接口
 */
@Component
public interface ProjectAlarmMapper {
    
    /**
     * 查询项目报警日志列表
     * @param param
     * @return
     * @throws Exception
     */
    public List<ProjectAlarm> query(ProjectAlarmQuery param) throws Exception;
    
    /**
     * 查询项目报警日志总数
     * @param param
     * @return
     * @throws Exception
     */
    public Integer queryCount(ProjectAlarmQuery param) throws Exception;
    
    /**
     * 清除报警日志
     * @param param
     * @return
     * @throws Exception
     */
    public int clear(ProjectAlarmQuery param) throws Exception;
        
    /**
     * 触发报警
     * @param log
     * @return
     * @throws Exception
     */
    public int trigger(ProjectAlarm log) throws Exception;
    
    /**
     * 解除报警
     * @param log
     * @return
     * @throws Exception
     */
    public int release(ProjectAlarm log) throws Exception;
}
