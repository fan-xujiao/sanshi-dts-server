package com.dts.server.db.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.dts.server.db.entity.MyTemplateProp;
import com.dts.server.dbutil.entity.TemplateProp;

public interface HistoryTableMapper {

	/**
	 * 创建历史数据表
	 * @param id
	 * @param props
	 * @return
	 * @throws Exception
	 */
	public int createTable(@Param("id")Integer id, @Param("props")List<TemplateProp> props) throws Exception;
	
	/**
	 * 删除历史数据表
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int dropTable(Integer id) throws Exception;
	
	/**
	 * 向历史数据表中增加一个字段
	 * @param id
	 * @param props
	 * @return
	 * @throws Exception
	 */
	public int addColumn(@Param("id")Integer id, @Param("prop")TemplateProp prop) throws Exception;
	
	/**
	 * 编辑历史数据表中一个字段
	 * @param id
	 * @param props
	 * @return
	 * @throws Exception
	 */
	public int editColumn(@Param("id")Integer id, @Param("prop")MyTemplateProp prop) throws Exception;
	
	/**
	 * 删除历史数据表中一个字段
	 * @param id
	 * @param propKey
	 * @return
	 * @throws Exception
	 */
	public int dropColumn(@Param("id")Integer id, @Param("propKey")String propKey) throws Exception;
}
