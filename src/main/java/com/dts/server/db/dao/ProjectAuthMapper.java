package com.dts.server.db.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.dts.server.dbutil.entity.ProjectAuth;
import com.dts.server.dbutil.entity.ProjectAuthKey;
import com.dts.server.query.AuthQuery;

/**
 * @ClassName:  ProjectAuthMapper
 * @author 范旭蛟
 * @date 2021年10月2日 下午8:42:42
 * @Description: 项目授权sql映射接口
 */
@Component
public interface ProjectAuthMapper {
    
    /**
     * 查询授权用户列表
     * @param param
     * @return
     * @throws Exception
     */
    public List<ProjectAuth> query(AuthQuery param) throws Exception;
    
    /**
     * 查询授权总数
     * @param param
     * @return
     * @throws Exception
     */
    public Integer queryCount(AuthQuery param) throws Exception;
    
    /**
     * 查询授权
     * @param id
     * @return
     * @throws Exception
     */
    public ProjectAuth selectById(ProjectAuthKey id) throws Exception;

    /**
     * 删除授权
     * @param id
     * @return
     * @throws Exception
     */
    public int delete(ProjectAuthKey id) throws Exception;
        
    /**
     * 新增授权
     * @param auth
     * @return
     * @throws Exception
     */
    public int add(ProjectAuth auth) throws Exception;
    
    /**
     * 编辑授权信息
     * @param auth
     * @return
     * @throws Exception
     */
    public int edit(ProjectAuth auth) throws Exception;
    
}
