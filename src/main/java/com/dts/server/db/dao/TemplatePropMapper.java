package com.dts.server.db.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.dts.server.dbutil.entity.TemplateProp;

/**
 * @ClassName:  TemplateMapper
 * @author 范旭蛟
 * @date 2021年10月17日 下午4:19:20
 * @Description: 监听模板属性sql映射接口
 */
@Component
public interface TemplatePropMapper {
    
    /**
     * 查询
     * @param param
     * @return
     * @throws Exception
     */
    public List<TemplateProp> query(Integer templateId) throws Exception;
    
    /**
     * 查询属性信息
     * @param id
     * @return
     * @throws Exception
     */
    public TemplateProp selectById(Integer id) throws Exception;
    
    /**
     * 删除
     * @param id
     * @return
     * @throws Exception
     */
    public int delete(Integer id) throws Exception;
    
    /**
     * 新增
     * @param prop
     * @throws Exception
     */
    public int add(TemplateProp prop) throws Exception;
    
    /**
     * 编辑
     * @param prop
     * @throws Exception
     */
    public int edit(TemplateProp prop) throws Exception;
}
