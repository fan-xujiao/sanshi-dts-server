# sanshi-dts-server

#### 介绍

三石数字孪生系统是一款用来对各种类型设备，进行数据采集，分析、预警的监视系统  
三石数字孪生系统服务端实现了系统使用的各种接口包括：  
用户的新增、编辑、删除、查询、登录等接口  
设备模板、属性、触发器等的查询、新增、编辑、删除等接口  
项目的查询、新增、编辑、删除、添加监听、授权等接口
监听数据的上报、查询等接口  

#### 软件架构

1.  整个系统包含了前端、服务端，已经数据采集客户端，三个部分，本工程只是服务端，需要配置其他两个工程，才可以正常使用
2.  编程语言：java
3.  使用的框架是：springboot+mybatis
4.  数据库：mysql
5.  IDE使用的是Eclipse
6.  构建工具用的maven
7.  jdk的版本是1.8
8.  tomcat的版本是8.5

#### 安装教程

1.  安装mysql数据库，我使用的是mysql8，建议不低于mysql5.6
2.  在mysql数据库中创建两个库：dts、dts_history, 字符集选择utf-8
3.  将项目中db.sql导入到dts库中
4.  安装jdk1.8 和tomcat8.5
5.  拉取代码，application.properties中的属性spring.profiles.active改为dev，并且将application-dev.properties中的数据库的配置修改
6.  打成war，部署到tomcat中，启动tomcat即可

#### 使用说明

1.  本项目只是服务端，需要配合中前端工程和数采客户端才可以正常使用
2.  前端工程的项目地址：
3.  数据采集客户端的地址：
4.  项目接口说明文档地址：

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

